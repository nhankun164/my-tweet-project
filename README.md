Welcome to my Tweet Application!
After you clone the source code, please run:

1. npm install
    * You must to run this command to download node_modules for project.
    * Project need to run on Angular 7 and contain Jasmine/Karma to unit testing.
    
2. ng serve --open
    * You will run the application by this command
    
3. ng test
    * You will see unit test result by this command.
    
    
The code process was built in my-tweet-project/tweet-app/src/app:
     * app.component.html: contain HTML code for my Tweet App
     * app.component.ts: contain the functions and variables, where I defined splitMessage() function.
     * app.component.css: contain CSS code to build styles for my Tweet App
     * app.component.spec.ts: contain unit test functions to test app.component.ts
 