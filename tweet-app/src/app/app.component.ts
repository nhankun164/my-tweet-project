import { Component } from '@angular/core';


export class MessageChunk {
  message: string[] = [];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'Tweet Application';
  messageStore: MessageChunk[] = [];
  chunk: MessageChunk = {} as MessageChunk;
  input: string;
  hasError = false;

  sendMessage() {
    this.hasError = false;
    this.splitMessage(this.input);
    this.input = '';
  }

  splitMessage(msg: string) {
    this.chunk.message = [];
    if (msg.length <= 50) {
      this.chunk.message.push(msg);
    } else {
      const sizeChunk = Math.ceil(msg.length / 50);
      let k = 1;
      // split string into chunk and add temporary part indicator into every element in chunk
      // temporary part indicator example : 1/##
      while (msg.length > (50 - `${k}/${sizeChunk}`.length)) {
          let pos = msg.substring(0, 50 - `${k}/${sizeChunk}`.length).lastIndexOf(' ');
          if (pos <= 0) {
            this.hasError = true;
            return;
          }
          const shortString = `${k}/${'#'.repeat(sizeChunk)} ${msg.substring(0, pos)}`;
          this.chunk.message.push(shortString);
          let i = msg.indexOf(' ', pos) + 1;
          if (i < pos || i > pos + 50) {
            i = pos;
          }
          msg = msg.substring(i);
          k++;
      }
      if (msg.trim() !== '') {
        this.chunk.message.push(`${k}/${sizeChunk} ${msg}`);
      }
      const newString: string[] = [];
      // replace temporary part indicator to correct part indicator
      this.chunk.message.map(x => {
        x = x.replace(x.substring(x.indexOf('/') + 1, x.indexOf(' ')), `${k}`).slice(0);
        newString.push(x);
      });
      this.chunk.message = [...newString];
    }
    const cloneChunk = Object.assign({}, this.chunk);
    this.messageStore.push(cloneChunk);
  }
}
