import { BrowserModule } from '@angular/platform-browser';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent, MessageChunk } from './app.component';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        BrowserModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should call event Click button and return the short message', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.input = 'aaaaaaaa';
    const chunkMock = {message: ['aaaaaaaa']} as MessageChunk;
    const messageStoreMock = [chunkMock] as MessageChunk[];
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(app.messageStore).toEqual(messageStoreMock);

  });

  it('should call event Click button and return the message chunk include 2 elements', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.input = 'I can\'t believe Tweeter now supports chunking my messages, so I don\'t have to do it myself.';
    const outputChunk1 = '1/2 I can\'t believe Tweeter now supports chunking';
    const outputChunk2 = '2/2 my messages, so I don\'t have to do it myself.';
    const chunkMock = {message: [outputChunk1, outputChunk2]} as MessageChunk;
    const messageStoreMock = [chunkMock] as MessageChunk[];
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(app.messageStore).toEqual(messageStoreMock);
  });

  it('should call event Click button and return error because of too long word', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    app.input = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    expect(app.hasError).toBeTruthy();
    const compiled = fixture.debugElement.nativeElement;
  });

  it(`should have as title 'Tweet Application'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Tweet Application');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Tweet Application!');
  });
});
